﻿// BattleForge Special Effect Editor
// Copyright(C) 2022 Skylords Reborn
// Project licensed under GNU General Public License v3.0. See LICENSE for more information.

using BattleForgeEffectEditor.Application.Commands;
using BattleForgeEffectEditor.Application.Settings;
using BattleForgeEffectEditor.Application.Utility;
using BattleForgeEffectEditor.Models.DataAccess;
using System;
using System.IO;
using System.Windows.Input;

namespace BattleForgeEffectEditor.Application.ViewModel
{
    public class SpecialEffectTopBarViewModel : ObservableObject
    {
        public ICommand SaveCommand => new RelayCommand((_) => Save());

        public ICommand SaveAsCommand => new RelayCommand((_) => SaveAs());

        public ICommand SaveAsBackupCommand => new RelayCommand((_) => SaveAsBackup(), (_) => CanSaveAsBackup());

        public ICommand OpenFileExplorerCommand => new RelayCommand((_) => OpenFileExplorer(), (_) => IsOpenFileExplorerEnabled);

        public ICommand UnhideAllElementsCommand => new RelayCommand((_) => UnhideAllElements());

        public bool IsOpenFileExplorerEnabled => File.Exists(effectEditor.FullFilePath);

        private SpecialEffectEditorViewModel effectEditor;

        private SpecialEffectExportService exportService = new SpecialEffectExportService();
        private DialogService dialogService = new DialogService();
        private WindowsService windowsService = new WindowsService();
        private SettingsService settingsService = new SettingsService();

        public SpecialEffectTopBarViewModel(SpecialEffectEditorViewModel effectEditor)
        {
            this.effectEditor = effectEditor;
        }

        private void Save()
        {
            if (string.IsNullOrEmpty(effectEditor.FullFilePath))
                SaveAs();
            else
                ExportTo(effectEditor.FullFilePath);
        }

        private void SaveAs()
        {
            string savePath = dialogService.SaveAsDialog("Save special effect as", "Fxb|*.fxb", "fxb");
            ExportTo(savePath);
        }

        /// <summary>
        /// For a special effect to be saved as a backup it must be initially saved and
        /// have a valid file path.
        /// </summary>
        /// <returns>True if it can be saved as backup, False otherwise.</returns>
        private bool CanSaveAsBackup()
        {
            return !string.IsNullOrEmpty(effectEditor.FullFilePath);
        }

        private void SaveAsBackup()
        {
            string backupDir = settingsService.GetBackupDirectory();

            if (string.IsNullOrEmpty(backupDir))
            {
                if (dialogService.ShowAreYouSure("No backup directory",
                    "No backup directory has been set yet, do you want to set it now?"))
                {
                    backupDir = dialogService.OpenDirectoryDialog("Choose backup directory.");

                    if (string.IsNullOrEmpty(backupDir))
                        return;

                    settingsService.SetBackupDirectory(backupDir);
                }
                else return;
            }

            var savePath = PathUtilities.GetBackupFileName(backupDir, effectEditor.FullFilePath);

            try
            {
                File.Copy(effectEditor.FullFilePath, savePath);

                dialogService.ShowOK("Backup special effect", "Backup created");
            }
            catch (Exception ex)
            {
                dialogService.ShowError("Error creating backup", ex.Message);
            }
        }

        public void OpenFileExplorer() => windowsService.TryOpenExplorerOnFile(effectEditor.FullFilePath);

        private void ExportTo(string fullFilePath)
        {
            if (fullFilePath == string.Empty)
                return;

            try
            {
                exportService.ExportEffect(effectEditor.SpecialEffect, fullFilePath);

                effectEditor.FullFilePath = fullFilePath;
                effectEditor.Title = Path.GetFileName(fullFilePath);
                RaisePropertyChanged(() => IsOpenFileExplorerEnabled);

                if (settingsService.GetFocusMapEditorOnSave())
                {
                    windowsService.PlayExclamationSound();
                    windowsService.PullUpAndFocusWindowsOfProcess("pluginbasededitor");
                } else
                    dialogService.ShowOK("Export successful", "Successfully exported fxb.");
            } catch (Exception e)
            {
                dialogService.ShowError("Failed to export fxb", e.Message);
            }
        }

        private void UnhideAllElements()
        {
            foreach (ElementTreeItemViewModel treeItem in effectEditor.ElementTree.Elements)
                treeItem.RecursiveSetElementEnable(true);

            foreach (TrackListItemViewModel track in effectEditor.ElementEditor.TracksList.Tracks)
                track.IsTrackEnabled = true;
        }
    }
}
