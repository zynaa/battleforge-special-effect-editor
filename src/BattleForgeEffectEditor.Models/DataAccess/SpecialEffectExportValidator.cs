﻿// BattleForge Special Effect Editor
// Copyright(C) 2022 Skylords Reborn
// Project licensed under GNU General Public License v3.0. See LICENSE for more information.

using BattleForgeEffectEditor.Models.Elements;
using BattleForgeEffectEditor.Models.Utility;

namespace BattleForgeEffectEditor.Models.DataAccess
{
    public class SpecialEffectExportValidator
    {
        public void ValidateSpecialEffect(SpecialEffect effect)
        {
            ValidateTracksHaveAtleastOneEntryKeyframe(effect);
        }

        private void ValidateTracksHaveAtleastOneEntryKeyframe(SpecialEffect effect)
        {
            foreach (IElement element in effect)
            {
                foreach (Track track in element.Tracks)
                {
                    if (track.EntryKeyFrames.Count != 0)
                        continue;

                    throw new UnexpectedDataException(
                        $"Track {track.TrackType} in element {element.Name} must have at least one frame!");
                }
            }
        }
    }
}
